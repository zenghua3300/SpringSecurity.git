package com.us.example.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//申明成Spring的组件
@Component("imoocAuthenticationSuccessHandler")
public class ImoocAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        logger.info("登录成功");
//        redisService.set("springsecurity3","123");
//        redisService.set("springsecurity3"+":"+jwtUser.getUsername(),jwtUserString);
//        if(LoginType.JSON.equals(securityProperties.getBrowser().getLoginType())){
//            response.setContentType("application/json;charset=UTF-8");
//            //把authentication对象转成json格式的字符串，
//            //然后以application/json的形式写会到响应里边去
//            response.getWriter().write(objectMapper.writeValueAsString(authentication));
//
//        }else{
//            //父类的方法就是跳转
//            super.onAuthenticationSuccess(request,response,authentication);
//
//        }

    }
}
