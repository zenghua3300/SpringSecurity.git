package com.us.example.filter;


import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.us.example.domain.JwtUser;
import com.us.example.model.LoginUser;
import com.us.example.service.RedisService;
import com.us.example.utils.JwtTokenUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private String password;

    private ThreadLocal<Integer> rememberMe = new ThreadLocal<>();
    private AuthenticationManager authenticationManager;

    ImoocAuthenticationSuccessHandler successHandler = new ImoocAuthenticationSuccessHandler();

    @Autowired
    RedisService redisService;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        super.setFilterProcessesUrl("/auth/login");
    }
    public JWTAuthenticationFilter() {
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {
        LoginUser loginUser = new LoginUser();

        try {
            // 从表单中获取用户信息
            if(request.getContentType().equals("application/x-www-form-urlencoded")){
                String username = request.getParameter("username");
                password = request.getParameter("password");

                loginUser.setUsername(username);
                loginUser.setPassword(password);
                loginUser.setRememberMe(1);

            }else{
                // 从输入流中获取用户信息
                InputStream inputStream= request.getInputStream();
                loginUser = new ObjectMapper().readValue(inputStream, LoginUser.class);

            }
            rememberMe.set(loginUser.getRememberMe());


            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword(), new ArrayList<>())
            );


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // 成功验证后调用的方法
    // 如果验证成功，就生成token并返回
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

        System.out.println(authResult.getPrincipal().toString());
        JwtUser jwtUser = (JwtUser) authResult.getPrincipal();
        System.out.println("jwtUser:" + jwtUser.toString());
        boolean isRemember = rememberMe.get() == 1;

        List permissionurl_methodList = new ArrayList();
        String role ="";
        Collection<? extends GrantedAuthority> authorities = jwtUser.getAuthorities();
        for (GrantedAuthority authority : authorities){
            role = authority.getAuthority();
            permissionurl_methodList.add(role);
        }

        String token = JwtTokenUtils.createToken(jwtUser.getUsername(), permissionurl_methodList, isRemember);

        String jwtUserString = JSONObject.toJSONString(jwtUser);

        //在过滤器中初始化bean
        BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        redisService= (RedisService) factory.getBean("redisService");
        redisService.set("springsecurity3"+":"+jwtUser.getUsername(),jwtUserString);

//        String token = JwtTokenUtils.createToken(jwtUser.getUsername(), false);
        // 返回创建成功的token
        // 但是这里创建的token只是单纯的token
        // 按照jwt的规定，最后请求的时候应该是 `Bearer token`
        response.setHeader("token", JwtTokenUtils.TOKEN_PREFIX + token);
        successHandler.onAuthenticationSuccess(request, response, authResult);

    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        response.getWriter().write("authentication failed, reason: " + failed.getMessage());
    }

}
