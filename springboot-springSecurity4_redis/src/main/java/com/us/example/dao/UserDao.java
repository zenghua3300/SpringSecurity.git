package com.us.example.dao;

import com.us.example.domain.JwtUser;
import com.us.example.domain.SysUser;


public interface UserDao {
    public JwtUser findByUserName(String username);
}
