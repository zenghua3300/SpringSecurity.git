package com.us.example.config;

import com.us.example.filter.JWTAuthenticationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration

public class WebMvcConfig extends WebMvcConfigurerAdapter{
//    @Bean
//    public JWTAuthenticationFilter jwtAuthenticationFilter(){
//        return new JWTAuthenticationFilter();
//    }
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }
//    @Override
//    public void addInterceptors(InterceptorRegistry registry){
//        //无关代码略去
//        registry.addInterceptor((HandlerInterceptor) jwtAuthenticationFilter());
//    }
}
