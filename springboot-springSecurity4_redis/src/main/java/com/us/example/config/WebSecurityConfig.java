package com.us.example.config;

import com.us.example.exception.JWTAccessDeniedHandler;
import com.us.example.exception.JWTAuthenticationEntryPoint;
import com.us.example.filter.JWTAuthenticationFilter;
import com.us.example.filter.JWTAuthorizationFilter;
import com.us.example.service.CustomUserService;
import com.us.example.service.MyFilterSecurityInterceptor;
import org.aspectj.weaver.ast.And;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.Filter;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;

    @Autowired
    private  CustomUserService customUserService;

    @Autowired
    private AuthenticationSuccessHandler imoocAuthenticationSuccessHandler;

//    @Autowired
//    private AuthenticationManager authenticationManager;

    //这里使用BCryptPasswordEncoder加密
    @Autowired
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customUserService).passwordEncoder(new BCryptPasswordEncoder());
    }


//    @Bean
//    public JWTAuthenticationFilter jwtAuthenticationFilter(AuthenticationManager authenticationManager)
//    {
//        return new JWTAuthenticationFilter(authenticationManager);
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

//        JWTAuthenticationFilter jwtAuthenticationFilter = new JWTAuthenticationFilter(authenticationManager());

//        jwtAuthenticationFilter.afterPropertiesSet();


        http.authorizeRequests()
                .antMatchers("/css/**", "/js/**","/images/**", "**/favicon.ico","/login.html").permitAll()
                .anyRequest().authenticated() //任何请求,登录后可以访问
                .and()
                .formLogin()
                .loginPage("/login")
                // 登录接口
//                .loginProcessingUrl("/auth/login")
                .successHandler(imoocAuthenticationSuccessHandler)
                .failureUrl("/login?error")
                .permitAll()
                .and()
                .logout()
                .permitAll() //注销行为任意访问
                .and()
                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
//                .exceptionHandling().authenticationEntryPoint(new JWTAuthenticationEntryPoint())
//                .accessDeniedHandler(new JWTAccessDeniedHandler());      //添加无权限时的处理
                .addFilter(new JWTAuthorizationFilter(authenticationManager()));
        http.addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class)
                .csrf().disable();
    }
//        public void configure(WebSecurity web) throws Exception {
//        //解决静态资源被拦截的问题
//        web.ignoring().antMatchers("/css/**");
//    }
}

