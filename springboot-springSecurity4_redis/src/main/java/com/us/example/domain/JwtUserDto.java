package com.us.example.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class JwtUserDto {

    private Integer id;
    private String username;
    private String password;
    private Collection authorities;

    public JwtUserDto(String username, String password, Collection authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public JwtUserDto() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Collection authorities) {
        this.authorities = authorities;
    }
}
