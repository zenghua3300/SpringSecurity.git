package com.us.example.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.us.example.exception.TokenIsExpiredException;
import com.us.example.security.UrlGrantedAuthority;
import com.us.example.utils.JwtTokenUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
    UrlGrantedAuthority urlGrantedAuthority = new UrlGrantedAuthority();

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        String tokenHeader = request.getHeader(JwtTokenUtils.TOKEN_HEADER);
        // 如果请求头中没有Authorization信息则直接放行了
        if (tokenHeader == null || !tokenHeader.startsWith(JwtTokenUtils.TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }
        // 如果请求头中有token，则进行解析，并且设置认证信息
        try {
            SecurityContextHolder.getContext().setAuthentication(getAuthentication(tokenHeader));
        } catch (TokenIsExpiredException e) {
            //返回json形式的错误信息
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            String reason = "统一处理，原因：" + e.getMessage();
            response.getWriter().write(new ObjectMapper().writeValueAsString(reason));
            response.getWriter().flush();
            return;
        }
        super.doFilterInternal(request, response, chain);
    }

    // 这里从token中获取用户信息并新建一个token
    private UsernamePasswordAuthenticationToken getAuthentication(String tokenHeader) throws TokenIsExpiredException {
        String token = tokenHeader.replace(JwtTokenUtils.TOKEN_PREFIX, "");
        boolean expiration = JwtTokenUtils.isExpiration(token);

        if (expiration) {
            throw new TokenIsExpiredException("token超时了");
        } else {
            String username = JwtTokenUtils.getUsername(token);
            List role = JwtTokenUtils.getUserRole(token);
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            //把role里的值取出来，然后组装成UrlGrantedAuthority
            for (int i =0;i<role.size();i++) {
                if (role.get(i) != null) {
                    urlGrantedAuthority = SplitRole((String)role.get(i));

                    GrantedAuthority grantedAuthority = new UrlGrantedAuthority(urlGrantedAuthority.getPermissionUrl(),urlGrantedAuthority.getMethod());

                    grantedAuthorities.add(grantedAuthority);
                }
            }
            if (username != null) {
                return new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);
            }
        }
        return null;
    }

    private UrlGrantedAuthority SplitRole(String urlG){

        String[] roleString = StringUtils.split(urlG,";");
        urlGrantedAuthority.setPermissionUrl(roleString[0]);
        urlGrantedAuthority.setMethod(roleString[1]);

        return urlGrantedAuthority;
    }
}
